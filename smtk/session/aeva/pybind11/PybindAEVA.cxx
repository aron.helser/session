//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include <pybind11/pybind11.h>
#include <utility>

// Include type-casters *before* any Pybind*.h headers.
#include "smtk/common/pybind11/PybindUUIDTypeCaster.h"
#include "smtk/extension/vtk/pybind11/PybindVTKTypeCaster.h"

namespace py = pybind11;

template <typename T, typename... Args>
using PySharedPtrClass = py::class_<T, std::shared_ptr<T>, Args...>;

// #include "PybindNameManager.h"
// #include "PybindRegisterStyleFunction.h"
#include "PybindCellSelection.h"
#include "PybindResource.h"
#include "PybindRegistrar.h"
// #include "PybindOperation.h"
#include "PybindPredicates.h"
#include "PybindSession.h"
#include "PybindGeometry.h"
#include "PybindVersion.h"

PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);

PYBIND11_MODULE(_smtkPybindAEVA, m)
{
  m.doc() = "<description of aeva>";
  //py::module smtk = m.def_submodule("smtk", "<description>");
  //py::module session = smtk.def_submodule("session", "<description>");
  //py::module aeva = session.def_submodule("aeva", "<description>");

  py::module::import("smtk.model");
  py::module::import("smtk.geometry");
  py::module::import("smtk.extension.vtk.geometry");

  // The order of these function calls is important! It was determined by
  // comparing the dependencies of each of the wrapped objects.
  py::class_< smtk::session::aeva::Version > smtk_session_aeva_Version = pybind11_init_smtk_session_aeva_Version(m);
  // py::class_< smtk::session::aeva::NameManager > smtk_session_aeva_NameManager = pybind11_init_smtk_session_aeva_NameManager(m);
  py::class_< smtk::session::aeva::Registrar > smtk_session_aeva_Registrar = pybind11_init_smtk_session_aeva_Registrar(m);
  pybind11_init_smtk_session_aeva_Iterate(m);
  pybind11_init_smtk_session_aeva_DimensionFromCellType(m);
  pybind11_init_smtk_session_aeva_EdgeNeighbors(m);
  pybind11_init_smtk_session_aeva_EntityType(m);
  pybind11_init_smtk_session_aeva_EstimateParametricDimension(m);
  pybind11_init_smtk_session_aeva_NumberOfCells(m);
  pybind11_init_smtk_session_aeva_SurfaceWithNormals(m);
  PySharedPtrClass< smtk::session::aeva::CellSelection, smtk::model::Entity > smtk_session_aeva_CellSelection = pybind11_init_smtk_session_aeva_CellSelection(m);
  // PySharedPtrClass< smtk::session::aeva::Geometry, smtk::geometry::Cache<smtk::extension::vtk::geometry::Geometry> > smtk_session_aeva_Geometry = pybind11_init_smtk_session_aeva_Geometry(m);
  // PySharedPtrClass< smtk::session::aeva::Operation, smtk::operation::XMLOperation > smtk_session_aeva_Operation = pybind11_init_smtk_session_aeva_Operation(m);
  // py::class_< smtk::session::aeva::RegisterStyleFunction, vtkSMTKRepresentationStyleSupplier<smtk::session::aeva::RegisterStyleFunction> > smtk_session_aeva_RegisterStyleFunction = pybind11_init_smtk_session_aeva_RegisterStyleFunction(m);
  PySharedPtrClass< smtk::session::aeva::Resource, smtk::resource::DerivedFrom<smtk::session::aeva::Resource, smtk::model::Resource> > smtk_session_aeva_Resource = pybind11_init_smtk_session_aeva_Resource(m);
  PySharedPtrClass< smtk::session::aeva::Session, smtk::model::Session > smtk_session_aeva_Session = pybind11_init_smtk_session_aeva_Session(m);
}
