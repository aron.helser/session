//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_BooleanSubtract_h
#define smtk_session_aeva_BooleanSubtract_h

#include "smtk/session/aeva/operators/Boolean.h"

namespace smtk
{
namespace session
{
namespace aeva
{

/// Intersect side sets by global ID.
class SMTKAEVASESSION_EXPORT BooleanSubtract : public Boolean
{
public:
  smtkTypeMacro(smtk::session::aeva::BooleanSubtract);
  smtkCreateMacro(BooleanSubtract);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(Boolean);

  Boolean::BooleanOpType type() const override { return Boolean::Subtract; }

protected:
  virtual const char* xmlDescription() const override;
};

} // namespace aeva
} // namespace session
} // namespace smtk

#endif // smtk_session_aeva_BooleanSubtract_h
