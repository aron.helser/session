<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "promotion from linear to quadratic" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="linear to quadratic promotion" Label="Linear to Quadratic Promotion" BaseType="operation">

      <BriefDescription>Generate a quadratic mesh promoted from a linear mesh.</BriefDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1">
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face|volume"/></Accepts>
      </AssociationsDef>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(quadratic promotion)" BaseType="result">
      <ItemDefinitions>
        <Void Name="allow camera reset" IsEnabledByDefault="true" AdvanceLevel="11"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
