<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the "create annotation resource" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="create annotation resource" Label="aeva annotation resource" BaseType="operation">

      <!-- User may optionally provide a geometric model resource to be annotated. -->
      <AssociationsDef
        Name="model resource"
        NumberOfRequiredValues="0"
        MaxNumberOfValues="1"
        Extensible="true"
        HoldReference="true">
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter=""/></Accepts>
        <BriefDescription>
          You may optionally provide an existing model resource on which annotations will be made.
        </BriefDescription>
        <DetailedDescription>
          The model resource on which annotations will be made.

          If provided, the annotation resource will be linked to the model resource.
          If not, a new aeva model resource will also be created and linked to the new aeva
          annotation resource.
        </DetailedDescription>
      </AssociationsDef>

      <ItemDefinitions>
      </ItemDefinitions>
    </AttDef>

    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(create annotation resource)" BaseType="result">
      <ItemDefinitions>
        <!-- Provide a way for resource managers observing this operation
             to fetch the resulting resource and take ownership of it
          -->
        <Resource Name="resource" HoldReference="true" NumberOfValues="1" Extensible="true">
          <Accepts>
            <Resource Name="smtk::attribute::Resource"/>
            <Resource Name="smtk::session::aeva::Resource"/>
          </Accepts>
        </Resource>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
