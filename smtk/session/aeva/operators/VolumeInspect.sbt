<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "volume inspect" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="volume inspect" Label="Volume Inspect" BaseType="operation">
      <BriefDescription>Create a vtkVolumeInspectionWidget</BriefDescription>
      <DetailedDescription>
        Create a vtkVolumeInspectionWidget, ie, 3 axis-aligned slice planes
      </DetailedDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1">
        <BriefDescription>The input volume </BriefDescription>
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="volume"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Group Name="3D slice planes" Label="Slice Planes" NumberOfGroups="1">
          <ItemDefinitions>
            <Double Name="origin point" Label="Origin Point" NumberOfRequiredValues="3">
              <BriefDescription>Origin point of 3 planes</BriefDescription>
              <DefaultValue>0, 0, 0</DefaultValue>
              <ComponentLabels>
                <Label>X</Label>
                <Label>Y</Label>
                <Label>Z</Label>
              </ComponentLabels>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(volume inspect)" BaseType="result"/>
  </Definitions>
  <Views>
    <View
      Type="Operation"
      Title="VolumeInspection Widget"
      TopLevel="true"
      FilterByAdvanceLevel="false"
      FilterByCategoryMode="false"
    >
      <InstancedAttributes>
        <Att Type="volume inspect" Name="volume inspect">
          <ItemViews>
            <View Item="3D slice planes"
              Type="VolumeInspectionPlanes"
              OriginPoint="origin point"
              ShowControls="true"/>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
