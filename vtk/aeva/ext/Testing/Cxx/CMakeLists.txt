#  Copyright (c) Kitware, Inc.  See License.md for details.

# vtk_add_test_cxx(AEVAExtCxxTests tests
#   NO_DATA NO_VALID NO_OUTPUT
#
#   TestVolumeInspector.cxx
# )
#
# vtk_test_cxx_executable(AEVAExtCxxTests tests)
#=============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
#=============================================================================

set (unit_tests
)

set (unit_tests_which_require_no_data
  TestVolumeInspector.cxx
)

set(external_libs
  ${ITK_LIBRARIES}
  VTK::CommonDataModel
  VTK::CommonTransforms
  VTK::FiltersCore
)

unit_tests(
  LABEL "AEVASession"
  SOURCES ${unit_tests}
  SOURCES_REQUIRE_DATA ${unit_tests_which_require_no_data}
  LIBRARIES smtkCore smtkAEVASession ${external_libs}
)
