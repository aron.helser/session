
//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "vtk/aeva/ext/vtkVolumeInspectionWidget.h"
#include "vtk/aeva/ext/vtkVolumeInspectionRepresentation.h"

#include "vtkCallbackCommand.h"
#include "vtkCamera.h"
#include "vtkCommand.h"
#include "vtkEvent.h"
#include "vtkObjectFactory.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkStdString.h"
#include "vtkWidgetCallbackMapper.h"
#include "vtkWidgetEvent.h"
#include "vtkWidgetEventTranslator.h"

vtkStandardNewMacro(vtkVolumeInspectionWidget);

//----------------------------------------------------------------------------
vtkVolumeInspectionWidget::vtkVolumeInspectionWidget()
{
  this->WidgetState = vtkVolumeInspectionWidget::Start;

  // Define widget events
  this->CallbackMapper->SetCallbackMethod(vtkCommand::LeftButtonPressEvent,
    vtkWidgetEvent::Select,
    this,
    vtkVolumeInspectionWidget::SelectAction);

  this->CallbackMapper->SetCallbackMethod(vtkCommand::LeftButtonReleaseEvent,
    vtkWidgetEvent::EndSelect,
    this,
    vtkVolumeInspectionWidget::EndSelectAction);

  this->CallbackMapper->SetCallbackMethod(vtkCommand::MiddleButtonPressEvent,
    vtkWidgetEvent::Translate,
    this,
    vtkVolumeInspectionWidget::TranslateAction);

  this->CallbackMapper->SetCallbackMethod(vtkCommand::MiddleButtonReleaseEvent,
    vtkWidgetEvent::EndTranslate,
    this,
    vtkVolumeInspectionWidget::EndSelectAction);

  this->CallbackMapper->SetCallbackMethod(
    vtkCommand::MouseMoveEvent, vtkWidgetEvent::Move, this, vtkVolumeInspectionWidget::MoveAction);
}

//----------------------------------------------------------------------------
vtkVolumeInspectionWidget::~vtkVolumeInspectionWidget() = default;

//----------------------------------------------------------------------
void vtkVolumeInspectionWidget::SelectAction(vtkAbstractWidget* w)
{
  auto* self = reinterpret_cast<vtkVolumeInspectionWidget*>(w);

  // Get the event position
  int X = self->Interactor->GetEventPosition()[0];
  int Y = self->Interactor->GetEventPosition()[1];

  // We want to compute an orthogonal vector to the plane that has been selected
  reinterpret_cast<vtkVolumeInspectionRepresentation*>(self->WidgetRep)
    ->SetInteractionState(vtkVolumeInspectionRepresentation::Moving);
  int interactionState = self->WidgetRep->ComputeInteractionState(X, Y);
  self->UpdateCursorShape(interactionState);

  if (self->WidgetRep->GetInteractionState() == vtkVolumeInspectionRepresentation::Outside)
  {
    return;
  }

  // We are definitely selected
  self->GrabFocus(self->EventCallbackCommand);
  double eventPos[2];
  eventPos[0] = static_cast<double>(X);
  eventPos[1] = static_cast<double>(Y);
  self->WidgetState = vtkVolumeInspectionWidget::Active;
  self->WidgetRep->StartWidgetInteraction(eventPos);

  self->EventCallbackCommand->SetAbortFlag(1);
  self->StartInteraction();
  self->InvokeEvent(vtkCommand::StartInteractionEvent, nullptr);
  self->Render();
}

//----------------------------------------------------------------------
void vtkVolumeInspectionWidget::TranslateAction(vtkAbstractWidget* w)
{
  auto* self = reinterpret_cast<vtkVolumeInspectionWidget*>(w);

  // Get the event position
  int X = self->Interactor->GetEventPosition()[0];
  int Y = self->Interactor->GetEventPosition()[1];

  // Okay, make sure that the pick is in the current renderer
  if (!self->CurrentRenderer || !self->CurrentRenderer->IsInViewport(X, Y))
  {
    self->WidgetState = vtkVolumeInspectionWidget::Start;
    return;
  }

  double eventPos[2];
  eventPos[0] = static_cast<double>(X);
  eventPos[1] = static_cast<double>(Y);
  self->WidgetRep->StartWidgetInteraction(eventPos);

  int interactionState = self->WidgetRep->GetInteractionState();
  if (interactionState == vtkVolumeInspectionRepresentation::Outside)
  {
    return;
  }

  self->WidgetState = vtkVolumeInspectionWidget::Active;
  self->GrabFocus(self->EventCallbackCommand);

  reinterpret_cast<vtkVolumeInspectionRepresentation*>(self->WidgetRep)
    ->SetInteractionState(vtkVolumeInspectionRepresentation::Moving);

  // We are definitely selected
  self->EventCallbackCommand->SetAbortFlag(1);
  self->StartInteraction();
  self->InvokeEvent(vtkCommand::StartInteractionEvent, nullptr);
  self->Render();
}

//----------------------------------------------------------------------
void vtkVolumeInspectionWidget::MoveAction(vtkAbstractWidget* w)
{
  auto* self = reinterpret_cast<vtkVolumeInspectionWidget*>(w);

  // So as to change the cursor shape when the mouse is poised over
  // the widget. Unfortunately, this results in a few extra picks
  // due to the cell picker. However given that its picking simple geometry
  // like the handles/arrows, this should be very quick
  int X = self->Interactor->GetEventPosition()[0];
  int Y = self->Interactor->GetEventPosition()[1];
  int changed = 0;

  if (self->ManagesCursor && self->WidgetState != vtkVolumeInspectionWidget::Active)
  {
    int oldInteractionState =
      reinterpret_cast<vtkVolumeInspectionRepresentation*>(self->WidgetRep)->GetInteractionState();

    reinterpret_cast<vtkVolumeInspectionRepresentation*>(self->WidgetRep)
      ->SetInteractionState(vtkVolumeInspectionRepresentation::Moving);
    int state = self->WidgetRep->ComputeInteractionState(X, Y);
    changed = self->UpdateCursorShape(state);
    reinterpret_cast<vtkVolumeInspectionRepresentation*>(self->WidgetRep)
      ->SetInteractionState(oldInteractionState);
    changed = (changed || state != oldInteractionState) ? 1 : 0;
  }

  // See whether we're active
  if (self->WidgetState == vtkVolumeInspectionWidget::Start)
  {
    if (changed && self->ManagesCursor)
    {
      self->Render();
    }
    return;
  }

  // Okay, adjust the representation
  double e[2];
  e[0] = static_cast<double>(X);
  e[1] = static_cast<double>(Y);
  self->WidgetRep->WidgetInteraction(e);

  // moving something
  self->EventCallbackCommand->SetAbortFlag(1);
  self->InvokeEvent(vtkCommand::InteractionEvent, nullptr);
  self->Render();
}

//----------------------------------------------------------------------
void vtkVolumeInspectionWidget::EndSelectAction(vtkAbstractWidget* w)
{
  auto* self = reinterpret_cast<vtkVolumeInspectionWidget*>(w);

  if (self->WidgetState != vtkVolumeInspectionWidget::Active ||
    self->WidgetRep->GetInteractionState() == vtkVolumeInspectionRepresentation::Outside)
  {
    return;
  }

  // Return state to not selected
  double e[2];
  self->WidgetRep->EndWidgetInteraction(e);
  self->WidgetState = vtkVolumeInspectionWidget::Start;
  self->ReleaseFocus();
  // reinterpret_cast<vtkVolumeInspectionRepresentation*>(self->WidgetRep)
  //     ->SetInteractionState(vtkVolumeInspectionRepresentation::Outside);

  // Update cursor if managed
  self->UpdateCursorShape(reinterpret_cast<vtkVolumeInspectionRepresentation*>(self->WidgetRep)
                            ->GetRepresentationState());

  self->EventCallbackCommand->SetAbortFlag(1);
  self->EndInteraction();
  self->InvokeEvent(vtkCommand::EndInteractionEvent, nullptr);
  self->Render();
}

//----------------------------------------------------------------------
void vtkVolumeInspectionWidget::SetEnabled(int enabling)
{
  if (this->Enabled == enabling)
  {
    return;
  }

  Superclass::SetEnabled(enabling);
}

//----------------------------------------------------------------------
void vtkVolumeInspectionWidget::CreateDefaultRepresentation()
{
  if (!this->WidgetRep)
  {
    this->WidgetRep = vtkVolumeInspectionRepresentation::New();
  }
}

//----------------------------------------------------------------------
void vtkVolumeInspectionWidget::SetRepresentation(vtkVolumeInspectionRepresentation* rep)
{
  this->Superclass::SetWidgetRepresentation(reinterpret_cast<vtkWidgetRepresentation*>(rep));
}

//----------------------------------------------------------------------
int vtkVolumeInspectionWidget::UpdateCursorShape(int state)
{
  // So as to change the cursor shape when the mouse is poised over
  // the widget.
  if (this->ManagesCursor)
  {
    if (state == vtkVolumeInspectionRepresentation::Outside)
    {
      return this->RequestCursorShape(VTK_CURSOR_DEFAULT);
    }
    return this->RequestCursorShape(VTK_CURSOR_HAND);
  }

  return 0;
}

//----------------------------------------------------------------------------
void vtkVolumeInspectionWidget::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
